﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestApiWS.Models
{
    /// <summary>
    /// Klasy do obsługi Magazynu (i nnych ogólnych recordSetów)
    //  To jest Univewlasny Element do obsługi danych otrzymanych w rekord set procedury SQL
    /// </summary>
    [DataContract]
    public class UnivElement
    {
        //ogólna nazwa dla kolumn otrzymanych z select
        [DataMember(Name = "C0")]
        public string C0 { get; set; }

        [DataMember(Name = "C1")]
        public string C1 { get; set; }

        [DataMember(Name = "C2")]
        public string C2 { get; set; }

        [DataMember(Name = "C3")]
        public string C3 { get; set; }

        [DataMember(Name = "C4")]
        public string C4 { get; set; }

        [DataMember(Name = "C5")]
        public string C5 { get; set; }

        //kolor elementu w formacie HEX RGB, bez # na początku
        [DataMember(Name = "Color")]
        public string Color { get; set; }
    }
}