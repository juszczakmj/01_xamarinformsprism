﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestApiWS.Models
{
    [DataContract]
    public partial class Exec
    {
        // Opis wykonania procedury
        [DataMember(Name = "Status")]
        public string Status { get; set; }

        // Status liczbowy wykonania procedury
        [DataMember(Name = "Code")]
        public int Code { get; set; }
    }
}