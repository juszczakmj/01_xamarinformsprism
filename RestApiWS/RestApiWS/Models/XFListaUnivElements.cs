﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestApiWS.Models
{
    /// <summary>
    /// Uniwersalna lista elementów dla 
    /// </summary>
    [DataContract]
    public class XFListaUnivElements:Exec
    {
        [DataMember(Name = "XFListaElements")]
        public List<XFUnivElement> ListaElements { get; set; }

        [DataMember(Name = "Tytul")]
        public string Tytul { get; set; }   //tytuł formatki do wyświetlenia na formatce

        //rozmiar czcionki do poszczególnej etykiety
        //--							wg wzorca: 
        //--							0 - etykieta E...niewykorzystywana
        //--							1 - duży rozmiar czcionki dla etykiety E...
        //--							2 - średni rozmiar czcionki dla etykiety E...
        //--							3 - mały rozmiar czcionki dla etykiety E...
        //--							4 - bardzo mały rozmiar czcionki dla etykiety E...
        //JUZ JEST INACZEJ: procedura zwraca konkretne wartości: large, medium, small, micro lub wielkość w pixelach
        [DataMember(Name = "Size1")]
        public string Size1 { get; set; }

        [DataMember(Name = "Size2")]
        public string Size2 { get; set; }

        [DataMember(Name = "Size3")]
        public string Size3 { get; set; }

        [DataMember(Name = "Size4")]
        public string Size4 { get; set; }

        [DataMember(Name = "Size5")]
        public string Size5 { get; set; }

        [DataMember(Name = "Size6")]
        public string Size6 { get; set; }

        [DataMember(Name = "Size7")]
        public string Size7 { get; set; }

        [DataMember(Name = "Size8")]
        public string Size8 { get; set; }

        [DataMember(Name = "Size9")]
        public string Size9 { get; set; }

        [DataMember(Name = "Size10")]
        public string Size10 { get; set; }


        //Atrybuty czionki dla etykiet (bold lub none)
        [DataMember(Name = "Atrib1")]
        public string Atrib1 { get; set; }

        [DataMember(Name = "Atrib2")]
        public string Atrib2 { get; set; }

        [DataMember(Name = "Atrib3")]
        public string Atrib3 { get; set; }

        [DataMember(Name = "Atrib4")]
        public string Atrib4 { get; set; }

        [DataMember(Name = "Atrib5")]
        public string Atrib5 { get; set; }

        [DataMember(Name = "Atrib6")]
        public string Atrib6 { get; set; }

        [DataMember(Name = "Atrib7")]
        public string Atrib7 { get; set; }

        [DataMember(Name = "Atrib8")]
        public string Atrib8 { get; set; }

        [DataMember(Name = "Atrib9")]
        public string Atrib9 { get; set; }

        [DataMember(Name = "Atrib10")]
        public string Atrib10 { get; set; }

        //Dodatkowy parametr, któy mozna wykorzystac do szczególnych ustawien
        public string Parametry_ust { get; set; }

        //konstruktor
        public XFListaUnivElements()
        {
            this.ListaElements = new List<XFUnivElement>();
            this.Tytul = "";

            this.Size1 = "0";
            this.Size2 = "0";
            this.Size3 = "0";
            this.Size4 = "0";
            this.Size5 = "0";
            this.Size6 = "0";
            this.Size7 = "0";
            this.Size8 = "0";
            this.Size9 = "0";
            this.Size10 = "0";

            this.Atrib1 = "none";
            this.Atrib2 = "none";
            this.Atrib3 = "none";
            this.Atrib4 = "none";
            this.Atrib5 = "none";
            this.Atrib6 = "none";
            this.Atrib7 = "none";
            this.Atrib8 = "none";
            this.Atrib9 = "none";
            this.Atrib10 = "none";

            this.Parametry_ust = "";
        }

    }
}