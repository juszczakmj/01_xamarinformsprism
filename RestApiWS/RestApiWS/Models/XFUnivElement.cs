﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestApiWS.Models
{
    /// <summary>
    /// Universalny element pod aplikację Xamarin Forms
    /// </summary>
    [DataContract]
    public class XFUnivElement
    {
        //ogólna nazwa dla kolumn otrzymanych z select
        [DataMember(Name = "E1")]
        public string E1 { get; set; }

        [DataMember(Name = "E2")]
        public string E2 { get; set; }

        [DataMember(Name = "E3")]
        public string E3 { get; set; }

        [DataMember(Name = "E4")]
        public string E4 { get; set; }

        [DataMember(Name = "E5")]
        public string E5 { get; set; }

        [DataMember(Name = "E6")]
        public string E6 { get; set; }

        [DataMember(Name = "E7")]
        public string E7 { get; set; }

        [DataMember(Name = "E8")]
        public string E8 { get; set; }

        [DataMember(Name = "E9")]
        public string E9 { get; set; }

        [DataMember(Name = "E10")]
        public string E10 { get; set; }

        //wartosc biezaca do wykresu
        [DataMember(Name = "GraphValue1")]
        public string GraphValue1 { get; set; }

        //wartosc docelowa do wykresu
        [DataMember(Name = "GraphValue2")]
        public string GraphValue2 { get; set; }

        //wartosc koloru poczatkowego do wykresu
        [DataMember(Name = "GraphColor1")]
        public string GraphColor1 { get; set; }

        //wartosc koloru poczatkowego do wykresu
        [DataMember(Name = "GraphColor2")]
        public string GraphColor2 { get; set; }

        //kolor tla dla formatki lub jakiegoś elementu w formacie HEX RGB, bez # na początku
        [DataMember(Name = "Color")]
        public string Color { get; set; }


    }
}