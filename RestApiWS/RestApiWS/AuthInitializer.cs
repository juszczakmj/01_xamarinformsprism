﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using RestApiWS.Entities;

namespace RestApiWS
{
    public class AuthInitializer : DropCreateDatabaseIfModelChanges<AuthContext>    //CreateDatabaseIfNotExists<AuthContext>
    {
        protected override void Seed(AuthContext context)
        {
            base.Seed(context);

            if (context.Clients.FirstOrDefault(x => x.Id == "mobileClient") == null)
            {
                context.Clients.Add(new Client()
                {
                    Id = "mobileClient",
                    Secret = "ZDJiYzhmMzgtOTliNC00YWNiLWEzNjgtYjVhYTZiMDM5NTYz",
                    Name = "Mobile",
                    Active = true,
                    RefreshTokenLifeTime = 5256000,
                    AllowedOrigin = "*"
                });
            }
        }
    }
}