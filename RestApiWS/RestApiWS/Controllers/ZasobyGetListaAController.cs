﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestApiWS.DSTableAdapters;
using RestApiWS.Models;

namespace RestApiWS.Controllers
{
    public class ZasobyGetListaAController : ApiController
    {
        // GET: api/ZasobyGetListaA
        //[AcceptVerbs("GET")]
        //[Route("{1}")]  //ni działa
        [HttpGet]
        //public IEnumerable<UnivElement> Get()
        public ListaUnivElements Get()
        {
            //var zasobyList = new List<UnivElement>();
            var zasobyList = new ListaUnivElements();

            for (int i = 0; i < 10; i++)
            {
                var zasobOne = new UnivElement()
                {
                    C0 = $"Kolumna C0 {i}", //String.Format("Kolumna C0 {0}",i),
                    C1 = $"Kolumna C1 {i}",
                    C2 = $"Kolumna C2 {i}",
                    C3 = $"Kolumna C3 {i}",
                    C4 = $"Kolumna C4 {i}",
                    C5 = $"Kolumna C5 {i}",
                    Color = $"AAFF0{i}"
                };

                zasobyList.ListaElements.Add(zasobOne);
            }

            zasobyList.Tytul = "Controller ZasobyGetListaA";
            zasobyList.Code = 1;
            zasobyList.Status = "OK";
            zasobyList.Width0 = 100;
            zasobyList.Width1 = 100;
            zasobyList.Width2 = 100;
            zasobyList.Width3 = 100;
            zasobyList.Width4 = 100;
            zasobyList.Width5 = 100;

            return zasobyList;
        }

        /// <summary>
        /// ZaasobyGetListaA
        /// Przykładowy kod maszyny do testu '31000000300025'
        /// </summary>
        /// <param name="machineCode"></param>
        /// <param name="userID"></param>
        /// <param name="otherParam"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        // GET: api/ZasobyGetListaA/5
        //[AcceptVerbs("GET")]
        [HttpGet]
        //public ListaUnivElements Get(string machineCode, string userID, string otherParam, string guid)
        public ListaUnivElements Get(string machineCode) //, string userID, string otherParam, string guid)
        {
            //tmp
            string userID = "1";
            string otherParam = "brak";
            string guid = "MASZYNA_LOKALNA";
            //tmp end

            ListaUnivElements ret = new ListaUnivElements();
            

            string status = null;
            string tytul = null;
            string w0 = "0";
            string w1 = "0";
            string w2 = "0";
            string w3 = "0";
            string w4 = "0";
            string w5 = "0";

            DS.p_P_Zasoby_Get_Lista_ADataTable ds = new DS.p_P_Zasoby_Get_Lista_ADataTable();
            RestApiWS.DSTableAdapters.p_P_Zasoby_Get_Lista_ATableAdapter da = new RestApiWS.DSTableAdapters.p_P_Zasoby_Get_Lista_ATableAdapter();

            using (da.Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["PRODUKCJA"].ConnectionString))
            {
                try
                {
                    //da.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["PRODUKCJA"].ConnectionString;
                    da.Fill(ds, machineCode, Int16.Parse(userID), otherParam, guid, ref tytul, ref w0, ref w1, ref w2, ref w3, ref w4, ref w5, ref status);
                    //Code i Status to zazwyczaj dwie wart zwracane przez procedury
                    ret.Code = int.Parse(da.Adapter.SelectCommand.Parameters["@RETURN_VALUE"].Value.ToString());
                    ret.Status = da.Adapter.SelectCommand.Parameters["@STATUS_DESCRIPTION"].Value.ToString();
                    ret.Tytul = da.Adapter.SelectCommand.Parameters["@TUTUL_OKNA"].Value.ToString();
                    //zwróć szerokości kolumn dla poszczególnych kolumn
                    ret.Width0 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width0"].Value.ToString());
                    ret.Width1 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width1"].Value.ToString());
                    ret.Width2 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width2"].Value.ToString());
                    ret.Width3 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width3"].Value.ToString());
                    ret.Width4 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width4"].Value.ToString());
                    ret.Width5 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width5"].Value.ToString());

                    if (!ds.HasErrors && ds.Rows.Count > 0)
                    {
                        //teraz dodaj caly wylistowany RecordSet
                        //Pierwszy wiersz RecordSet zawiera nagłówki kolumn do listView
                        int rozmrows = ds.Rows.Count;
                        for (var i = 0; i < rozmrows; i++)
                        {
                            ret.ListaElements.Add(new UnivElement()
                            {
                                C0 = ds.Rows[i][0].ToString(),  //w Columns.List siedzą nagłówki kolumn
                                C1 = ds.Rows[i][1].ToString(),
                                C2 = ds.Rows[i][2].ToString(),
                                C3 = ds.Rows[i][3].ToString(),
                                C4 = ds.Rows[i][4].ToString(),
                                C5 = ds.Rows[i][5].ToString(),
                                Color = ds.Rows[i][6].ToString(),
                            });

                        }

                    }
                }
                catch (Exception ex)
                {
                    ret.Code = -1;
                    ret.Status = ex.Message;
                    ret.Tytul = da.Adapter.SelectCommand.Parameters["@TUTUL_OKNA"].Value.ToString();
                    //zwróć szerokości kolumn dla poszczególnych kolumn
                    //nie jestem pewien czy w catch jest to potrzebne, ale na wszelki wypadek na przyszłość
                    ret.Width0 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width0"].Value.ToString());
                    ret.Width1 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width1"].Value.ToString());
                    ret.Width2 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width2"].Value.ToString());
                    ret.Width3 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width3"].Value.ToString());
                    ret.Width4 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width4"].Value.ToString());
                    ret.Width5 = int.Parse(da.Adapter.SelectCommand.Parameters["@C_Width5"].Value.ToString());
                }
            }

            return ret;


        }

        // POST: api/ZasobyGetListaA
        [AcceptVerbs("POST")]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ZasobyGetListaA/5
        [AcceptVerbs("PUT")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ZasobyGetListaA/5
        [AcceptVerbs("DELETE")]
        public void Delete(int id)
        {
        }
    }
}
