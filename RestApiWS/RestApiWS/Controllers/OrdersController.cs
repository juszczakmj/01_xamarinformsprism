﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestApiWS.Controllers
{
    [RoutePrefix("api/Orders")]
    public class OrdersController : ApiController
    {
        [Authorize]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(Order.CreateOrders());
        }

    }

    #region Helpers

    public class Order
    {
        public int OrderID { get; set; }
        public string CustomerName { get; set; }
        public string ShipperCity { get; set; }
        public Boolean IsShipped { get; set; }

        public static List<Order> CreateOrders()
        {
            List<Order> OrderList = new List<Order>
            {
                new Order {OrderID = 10248, CustomerName = "Taiseer Joudeh", ShipperCity = "Garwolin", IsShipped = true },
                new Order {OrderID = 10249, CustomerName = "Mirek Juszczak", ShipperCity = "Garwolin", IsShipped = false},
                new Order {OrderID = 10250, CustomerName = "Marcin Dłużniewski", ShipperCity = "Warszawa", IsShipped = false },
                new Order {OrderID = 10251, CustomerName = "Jarek Szwaj", ShipperCity = "Warszawa", IsShipped = false},
                new Order {OrderID = 10252, CustomerName = "Tomek Owczuk", ShipperCity = "Warszawa", IsShipped = true}
            };

            return OrderList;
        }
    }

    #endregion
}
