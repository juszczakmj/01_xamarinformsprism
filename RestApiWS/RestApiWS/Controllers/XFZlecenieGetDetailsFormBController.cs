﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestApiWS.Models;

namespace RestApiWS.Controllers
{
    [Authorize]
    public class XFZlecenieGetDetailsFormBController : ApiController
    {
        // GET: api/XFZlecenieDetailsFormB
        [AcceptVerbs("GET")]
        public XFListaUnivElements Get()
        {
            //var zasobyList = new List<UnivElement>();
            var zasobyList = new XFListaUnivElements();

            for (int i = 0; i < 10; i++)
            {
                var zasobOne = new XFUnivElement()
                {
                    E1 = $"Kolumna E1 {i}", // ==String.Format("Kolumna E1 {0}",i),
                    E2 = $"Kolumna E2 {i}",
                    E3 = $"Kolumna E3 {i}",
                    E4 = $"Kolumna E4 {i}",
                    E5 = $"Kolumna E5 {i}",
                    E6 = $"Kolumna E6 {i}",
                    E7 = $"Kolumna E7 {i}",
                    E8 = $"Kolumna E8 {i}",
                    E9 = $"Kolumna E9 {i}",
                    E10 = $"Kolumna E10 {i}",
                    GraphValue1 = $"1000",
                    GraphValue2 = $"1500",
                    GraphColor1 = $"#FFFF0{i}",
                    GraphColor2 = $"#FFFF0{i}",
                    Color = $"#AAFF0{i}"        //przerabiam od razu na pełne RGB - łatwiej potem w View'sach
                };

                zasobyList.ListaElements.Add(zasobOne);
            }

            zasobyList.Tytul = "Controller XFZleceniaGetListaA";
            zasobyList.Code = 1;
            zasobyList.Status = "OK";

            zasobyList.Size1 = "Default";
            zasobyList.Size2 = "Default";
            zasobyList.Size3 = "Default";
            zasobyList.Size4 = "Default";
            zasobyList.Size5 = "Default";
            zasobyList.Size6 = "Default";
            zasobyList.Size7 = "Default";
            zasobyList.Size8 = "Default";
            zasobyList.Size9 = "Default";
            zasobyList.Size10 = "Default";

            zasobyList.Atrib1 = "None";
            zasobyList.Atrib2 = "None";
            zasobyList.Atrib3 = "None";
            zasobyList.Atrib4 = "None";
            zasobyList.Atrib5 = "None";
            zasobyList.Atrib6 = "None";
            zasobyList.Atrib7 = "None";
            zasobyList.Atrib8 = "None";
            zasobyList.Atrib9 = "None";
            zasobyList.Atrib10 = "None";

            return zasobyList;
        }

        // GET: api/XFZlecenieGetDetailsFormB/5
        [AcceptVerbs("GET")]
        public XFListaUnivElements Get(string zlecenieCode, string userID, string otherParam, string guid)
        {
            //tmp
            //string userID = "1";
            //string otherParam = "brak";
            //string guid = "Mobile";
            //tmp end

            XFListaUnivElements ret = new XFListaUnivElements();


            string status = null;
            string tytul = null;
            string s1 = "0";
            string s2 = "0";
            string s3 = "0";
            string s4 = "0";
            string s5 = "0";
            string s6 = "0";
            string s7 = "0";
            string s8 = "0";
            string s9 = "0";
            string s10 = "0";
            string a1 = "0";
            string a2 = "0";
            string a3 = "0";
            string a4 = "0";
            string a5 = "0";
            string a6 = "0";
            string a7 = "0";
            string a8 = "0";
            string a9 = "0";
            string a10 = "0";

            DS.p_P_XAM_Get_Zlecenie_Details_Form_BDataTable ds = new DS.p_P_XAM_Get_Zlecenie_Details_Form_BDataTable();
            RestApiWS.DSTableAdapters.p_P_XAM_Get_Zlecenie_Details_Form_BTableAdapter da = new RestApiWS.DSTableAdapters.p_P_XAM_Get_Zlecenie_Details_Form_BTableAdapter();

            using (da.Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["PRODUKCJA"].ConnectionString))
            {
                try
                {
                    //da.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["PRODUKCJA"].ConnectionString;
                    da.Fill(ds, zlecenieCode, Int16.Parse(userID), otherParam, guid, ref tytul, ref s1, ref s2, ref s3, ref s4, ref s5, ref s6, ref s7, ref s8, ref s9, ref s10, ref a1, ref a2, ref a3, ref a4, ref a5, ref a6, ref a7, ref a8, ref a9, ref a10, ref status);
                    //Code i Status to zazwyczaj dwie wart zwracane przez procedury
                    ret.Code = int.Parse(da.Adapter.SelectCommand.Parameters["@RETURN_VALUE"].Value.ToString());
                    ret.Status = da.Adapter.SelectCommand.Parameters["@STATUS_DESCRIPTION"].Value.ToString();
                    ret.Tytul = da.Adapter.SelectCommand.Parameters["@TUTUL_OKNA"].Value.ToString();

                    //zwróć wielkosc czcionki dla poszczególnych etykiet
                    ret.Size1 = da.Adapter.SelectCommand.Parameters["@E_Size1"].Value.ToString();
                    ret.Size2 = da.Adapter.SelectCommand.Parameters["@E_Size2"].Value.ToString();
                    ret.Size3 = da.Adapter.SelectCommand.Parameters["@E_Size3"].Value.ToString();
                    ret.Size4 = da.Adapter.SelectCommand.Parameters["@E_Size4"].Value.ToString();
                    ret.Size5 = da.Adapter.SelectCommand.Parameters["@E_Size5"].Value.ToString();
                    ret.Size6 = da.Adapter.SelectCommand.Parameters["@E_Size6"].Value.ToString();
                    ret.Size7 = da.Adapter.SelectCommand.Parameters["@E_Size7"].Value.ToString();
                    ret.Size8 = da.Adapter.SelectCommand.Parameters["@E_Size8"].Value.ToString();
                    ret.Size9 = da.Adapter.SelectCommand.Parameters["@E_Size9"].Value.ToString();
                    ret.Size10 = da.Adapter.SelectCommand.Parameters["@E_Size10"].Value.ToString();

                    //zwróć atrybuty czcionki dla poszczególnych etykiet
                    ret.Atrib1 = da.Adapter.SelectCommand.Parameters["@E_Atrib1"].Value.ToString();
                    ret.Atrib2 = da.Adapter.SelectCommand.Parameters["@E_Atrib2"].Value.ToString();
                    ret.Atrib3 = da.Adapter.SelectCommand.Parameters["@E_Atrib3"].Value.ToString();
                    ret.Atrib4 = da.Adapter.SelectCommand.Parameters["@E_Atrib4"].Value.ToString();
                    ret.Atrib5 = da.Adapter.SelectCommand.Parameters["@E_Atrib5"].Value.ToString();
                    ret.Atrib6 = da.Adapter.SelectCommand.Parameters["@E_Atrib6"].Value.ToString();
                    ret.Atrib7 = da.Adapter.SelectCommand.Parameters["@E_Atrib7"].Value.ToString();
                    ret.Atrib8 = da.Adapter.SelectCommand.Parameters["@E_Atrib8"].Value.ToString();
                    ret.Atrib9 = da.Adapter.SelectCommand.Parameters["@E_Atrib9"].Value.ToString();
                    ret.Atrib10 = da.Adapter.SelectCommand.Parameters["@E_Atrib10"].Value.ToString();

                    if (!ds.HasErrors && ds.Rows.Count > 0)
                    {
                        //teraz dodaj caly wylistowany RecordSet
                        //Pierwszy wiersz RecordSet zawiera nagłówki kolumn do listView
                        int rozmrows = ds.Rows.Count;
                        for (var i = 0; i < rozmrows; i++)
                        {
                            ret.ListaElements.Add(new XFUnivElement()
                            {
                                E1 = ds.Rows[i][0].ToString(),  //w Columns.List siedzą nagłówki kolumn
                                E2 = ds.Rows[i][1].ToString(),
                                E3 = ds.Rows[i][2].ToString(),
                                E4 = ds.Rows[i][3].ToString(),
                                E5 = ds.Rows[i][4].ToString(),
                                E6 = ds.Rows[i][5].ToString(),
                                E7 = ds.Rows[i][6].ToString(),
                                E8 = ds.Rows[i][7].ToString(),
                                E9 = ds.Rows[i][8].ToString(),
                                E10 = ds.Rows[i][9].ToString(),
                                GraphValue1 = ds.Rows[i][10].ToString(),
                                GraphValue2 = ds.Rows[i][11].ToString(),
                                GraphColor1 = String.Format("#{0}", ds.Rows[i][12].ToString()),
                                GraphColor2 = String.Format("#{0}", ds.Rows[i][13].ToString()),
                                Color = String.Format("#{0}", ds.Rows[i][14].ToString()),
                            });

                        }

                    }
                }
                catch (Exception ex)
                {
                    ret.Code = -1;
                    ret.Status = ex.Message;
                    ret.Tytul = da.Adapter.SelectCommand.Parameters["@TUTUL_OKNA"].Value.ToString();

                    //zwróć rozmiary fontów dla poszczególnych etykiet
                    ret.Size1 = da.Adapter.SelectCommand.Parameters["@E_Size1"].Value.ToString();
                    ret.Size2 = da.Adapter.SelectCommand.Parameters["@E_Size2"].Value.ToString();
                    ret.Size3 = da.Adapter.SelectCommand.Parameters["@E_Size3"].Value.ToString();
                    ret.Size4 = da.Adapter.SelectCommand.Parameters["@E_Size4"].Value.ToString();
                    ret.Size5 = da.Adapter.SelectCommand.Parameters["@E_Size5"].Value.ToString();
                    ret.Size6 = da.Adapter.SelectCommand.Parameters["@E_Size6"].Value.ToString();
                    ret.Size7 = da.Adapter.SelectCommand.Parameters["@E_Size7"].Value.ToString();
                    ret.Size8 = da.Adapter.SelectCommand.Parameters["@E_Size8"].Value.ToString();
                    ret.Size9 = da.Adapter.SelectCommand.Parameters["@E_Size9"].Value.ToString();
                    ret.Size10 = da.Adapter.SelectCommand.Parameters["@E_Size10"].Value.ToString();

                    //zwróć atrybuty czcionki dla poszczególnych etykiet
                    ret.Atrib1 = da.Adapter.SelectCommand.Parameters["@E_Atrib1"].Value.ToString();
                    ret.Atrib2 = da.Adapter.SelectCommand.Parameters["@E_Atrib2"].Value.ToString();
                    ret.Atrib3 = da.Adapter.SelectCommand.Parameters["@E_Atrib3"].Value.ToString();
                    ret.Atrib4 = da.Adapter.SelectCommand.Parameters["@E_Atrib4"].Value.ToString();
                    ret.Atrib5 = da.Adapter.SelectCommand.Parameters["@E_Atrib5"].Value.ToString();
                    ret.Atrib6 = da.Adapter.SelectCommand.Parameters["@E_Atrib6"].Value.ToString();
                    ret.Atrib7 = da.Adapter.SelectCommand.Parameters["@E_Atrib7"].Value.ToString();
                    ret.Atrib8 = da.Adapter.SelectCommand.Parameters["@E_Atrib8"].Value.ToString();
                    ret.Atrib9 = da.Adapter.SelectCommand.Parameters["@E_Atrib9"].Value.ToString();
                    ret.Atrib10 = da.Adapter.SelectCommand.Parameters["@E_Atrib10"].Value.ToString();
                }
            }

            return ret;


        }

        // POST: api/XFZlecenieDetailsFormB
        [AcceptVerbs("POST")]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/XFZlecenieDetailsFormB/5
        [AcceptVerbs("PUT")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/XFZlecenieDetailsFormB/5
        [AcceptVerbs("DELETE")]
        public void Delete(int id)
        {
        }
    }
}
