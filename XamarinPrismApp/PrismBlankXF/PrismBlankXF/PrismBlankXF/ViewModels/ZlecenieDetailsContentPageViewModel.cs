﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Prism.Navigation;
using Prism.Services;
using PrismBlankXF.Date.Rest;
using PrismBlankXF.Models;

namespace PrismBlankXF.ViewModels
{
	public class ZlecenieDetailsContentPageViewModel : BindableBase, INavigatedAware
	{

        //zmienna do obsługi polaczenia
	    private readonly MyRestDataServiceZlecenia _myrestDataService = new MyRestDataServiceZlecenia();

        private string _title;
	    public string Title
	    {
	        get { return _title; }
	        set { SetProperty(ref _title, value); }
	    }

	    XFListaUnivElements ListFromRestApi = new XFListaUnivElements();
  
        //zmienna do przechwycenia elementu listy
        XFUnivElement XFUnivElemDetails = new XFUnivElement();

	    public IPageDialogService _dialogService { get; set; }
	    private readonly INavigationService _navigationService;

        //zmienna do przechwycenia elementu listy
        //   private XFUnivElement _xfunivelemdetails;
        //public XFUnivElement XFUnivElemDetails
        //{
        //    get { return _xfunivelemdetails; }
        //    set { SetProperty(ref _xfunivelemdetails, value); }
        //}

        //ZMIENNE DO POSZCZEGOLNYCH FORMATEK WIDOKU
        //naglowki
        private ViewProperty _headerFormA;
	    public ViewProperty HeaderFormA
	    {
	        get { return _headerFormA; }
	        set { SetProperty(ref _headerFormA, value); }
	    }

        //Trzeba z HeaderFormA i HeadFormB przechwycic tylko obiekt XFUnivElem bo bindowanie nie działa na podobiektach
	    private XFUnivElement _labelsFormA;
	    public XFUnivElement LabelsFormA
        {
	        get { return _labelsFormA; }
	        set { SetProperty(ref _labelsFormA, value); }
	    }

	    private XFUnivElement _labelsFormB;
	    public XFUnivElement LabelsFormB
	    {
	        get { return _labelsFormB; }
	        set { SetProperty(ref _labelsFormB, value); }
	    }

        private ViewProperty _headerFormB;
	    public ViewProperty HeaderFormB
	    {
	        get { return _headerFormB; }
	        set { SetProperty(ref _headerFormB, value); }
	    }

        private ViewProperty _headerFormC;
	    public ViewProperty HeaderFormC
	    {
	        get { return _headerFormC; }
	        set { SetProperty(ref _headerFormC, value); }
	    }

	    private ViewProperty _headerFormD;
	    public ViewProperty HeaderFormD
	    {
	        get { return _headerFormD; }
	        set { SetProperty(ref _headerFormD, value); }
	    }

        //zmienne przechwytujace wyniki procedur - to beda dwuwierszowe sety: naglowek+rekord
	    private XFUnivElement _xfunivelemFormA;
	    public XFUnivElement XFUnivElemFormA
	    {
	        get { return _xfunivelemFormA; }
	        set { SetProperty(ref _xfunivelemFormA, value); }
	    }

	    private XFUnivElement _xfunivelemFormB;
	    public XFUnivElement XFUnivElemFormB
	    {
	        get { return _xfunivelemFormB;}
	        set { SetProperty(ref _xfunivelemFormB, value); }
	    }

	    private XFUnivElement _xfunivelemFormC;
	    public XFUnivElement XFUnivElemFormC
	    {
	        get { return _xfunivelemFormC; }
	        set { SetProperty(ref _xfunivelemFormC, value); }
	    }


        //FormD jest lista elementow
	    private List<XFUnivElement> _xflistelemFormD;
	    public List<XFUnivElement> XFListElemFormD
	    {
	        get { return _xflistelemFormD; }
	        set { SetProperty(ref _xflistelemFormD, value); }
	    }

        /// <summary>
        /// Zmienne do wykresu Procent, Colors
        /// </summary>
	    private int _procent;  //% do wyswietlenia na wykresie
	    public int Procent
	    {
	        get { return _procent; }
	        set { SetProperty(ref _procent, value); }
	    }

	    private int _colorstop1;  //% koloru do wyswietlenia na wykresie
	    public int Colorstop1
	    {
	        get { return _colorstop1; }
	        set { SetProperty(ref _colorstop1, value); }
	    }

	    private int _colorstop2;  //% koloru do wyswietlenia na wykresie
	    public int Colorstop2
	    {
	        get { return _colorstop2; }
	        set { SetProperty(ref _colorstop2, value); }
	    }

	    private int _graphInterwal;  //% koloru do wyswietlenia na wykresie
	    public int GraphInterwal
        {
	        get { return _graphInterwal; }
	        set { SetProperty(ref _graphInterwal, value); }
	    }
        /////KONIEC ZMIENNYCH DO FORMATEK WIDOKU

        /// <summary>
        /// konstruktor
        /// </summary>
	    public ZlecenieDetailsContentPageViewModel(INavigationService navigationService, IPageDialogService dialogService)
	    {
	        _navigationService = navigationService;

            //Zmienna do Msg
            _dialogService = dialogService;

            XFUnivElemFormA = new XFUnivElement();
            XFUnivElemFormB = new XFUnivElement();
            XFUnivElemFormC = new XFUnivElement();
            XFListElemFormD = new List<XFUnivElement>();
            HeaderFormA = new ViewProperty();
            HeaderFormB =new ViewProperty();
            HeaderFormC =new ViewProperty();
            HeaderFormD =new ViewProperty();

            LabelsFormA=new XFUnivElement();
            LabelsFormB=new XFUnivElement();

            //Procent = 1;        //tylko init
            //Colorstop1 = 1;     //tylko init
            //Colorstop2 = 1;     //tylko init
    }

	    public async void ShowMsg(string title, string mess, string cancel)
	    {
	        await _dialogService.DisplayAlertAsync(title, mess, cancel);

	    }

	    public void GoToZleceniaListPage()
	    {
	        _navigationService.NavigateAsync($"ZleceniaListContentPage");
	    }

	    public void GoToMainPage()
	    {
	        _navigationService.NavigateAsync($"MainPage");
	    }


        //async void GetFormAAndSetVariable(string zlecenieCode, string userID, string otherParam, string guid)
        async Task GetFormAAndSetVariable(string zlecenieCode, string userID, string otherParam, string guid)
        {

            ListFromRestApi = await _myrestDataService.XFZlecenieGetDetailsFormA(zlecenieCode, userID, otherParam, guid);

            //jesli lista jest pusta to nie pobrano danych z RestApi
            if (ListFromRestApi.ListaElements.Count!=0)
            {
                //zamien na "" jesli FontSize=0
                ListFromRestApi.ClearNullFieldOfList();

                //ustaw nagłowki etykiet i ich własności
                HeaderFormA.HeaderList = ListFromRestApi.ListaElements[0];     //nagłowki etykiet
                HeaderFormA.HeaderFontProperty = new ViewFontEtykProperty(ListFromRestApi);                        //własności fontów header
                LabelsFormA = HeaderFormA.HeaderList;

                //usun pierwszy odczytany wiersz - nagłowki
                ListFromRestApi.ListaElements.RemoveAt(0);

                //Wyczyszczenie zbędnych null i wrzucenie pierwszego wiersza listy do ViewProperty
                //ClearNullField(MyDatas);
                ListFromRestApi.ClearNullFieldOfList();

                //Do obiektów obserwowanych wstaw tylko drugi rekord - będzie sprawniej - po odcięciui 1-go elem to element 0
                XFUnivElemFormA = ListFromRestApi.ListaElements[0];

            }
            else
            {
                ShowMsg("Błąd: Formatka A", "Problem z dostępem do bazy danych. Sprawdź swoje połączenie i ponów próbę.", "OK");
                //GoToZleceniaListPage();
                GoToMainPage();

            }



        }

        //async void GetFormBAndSetVariable(string zlecenieCode, string userID, string otherParam, string guid)
        async Task GetFormBAndSetVariable(string zlecenieCode, string userID, string otherParam, string guid)
        {

	        ListFromRestApi = await _myrestDataService.XFZlecenieGetDetailsFormB(zlecenieCode, userID, otherParam, guid);

	        //jesli lista jest pusta to nie pobrano danych z RestApi
	        if (ListFromRestApi.ListaElements.Count != 0)
            {
                //zamien na "" jesli FontSize=0
                ListFromRestApi.ClearNullFieldOfList();

                //ustaw nagłowki etykiet i ich własności
                HeaderFormB.HeaderList = ListFromRestApi.ListaElements[0];     //nagłowki etykiet
	            HeaderFormB.HeaderFontProperty = new ViewFontEtykProperty(ListFromRestApi);                        //własności fontów header
                LabelsFormB = HeaderFormB.HeaderList;

                //usun pierwszy odczytany wiersz - nagłowki
                ListFromRestApi.ListaElements.RemoveAt(0);

	            //Wyczyszczenie zbędnych null i wrzucenie pierwszego wiersza listy do ViewProperty
	            //ClearNullField(MyDatas);
	            ListFromRestApi.ClearNullFieldOfList();

	            //Do obiektów obserwowanych wstaw tylko drugi rekord - będzie sprawniej - po odcięciui 1-go elem to element 0
	            XFUnivElemFormB = ListFromRestApi.ListaElements[0];
            }
	        else
	        {
                ShowMsg("Błąd: Formatka B", "Problem z dostępem do bazy danych. Sprawdź swoje połączenie i ponów próbę.", "OK");
                //GoToZleceniaListPage();
	            GoToMainPage();
            }



        }


        //async void GetFormCAndSetVariable(string zlecenieCode, string userID, string otherParam, string guid)
        async Task GetFormCAndSetVariable(string zlecenieCode, string userID, string otherParam, string guid)
        {

	        ListFromRestApi = await _myrestDataService.XFZlecenieGetDetailsFormC(zlecenieCode, userID, otherParam, guid);

	        //jesli lista jest pusta to nie pobrano danych z RestApi
	        if (ListFromRestApi.ListaElements.Count != 0)
            {
                //zamien na "" jesli FontSize=0
                ListFromRestApi.ClearNullFieldOfList();

                //ustaw nagłowki etykiet i ich własności
                HeaderFormC.HeaderList = ListFromRestApi.ListaElements[0];     //nagłowki etykiet
                HeaderFormC.HeaderFontProperty = new ViewFontEtykProperty(ListFromRestApi);                        //własności fontów header

                //usun pierwszy odczytany wiersz - nagłowki
                ListFromRestApi.ListaElements.RemoveAt(0);

                //Wyczyszczenie zbędnych null i wrzucenie pierwszego wiersza listy do ViewProperty
                //ClearNullField(MyDatas);
                ListFromRestApi.ClearNullFieldOfList();

                //Do obiektów obserwowanych wstaw tylko drugi rekord - będzie sprawniej - po odcięciui 1-go elem to element 0
                XFUnivElemFormC = ListFromRestApi.ListaElements[0];


                //ustawienie %-tow i wartosci do zmian koloru na wykresie
                Procent = (Int32.Parse(XFUnivElemFormC.GraphValue1) * 100) / Int32.Parse(XFUnivElemFormC.GraphValue2);
                Colorstop1 = (int)0.35 * Int32.Parse(XFUnivElemFormC.GraphValue2);
                Colorstop2 = (int)0.75 * Int32.Parse(XFUnivElemFormC.GraphValue2);
                GraphInterwal = (int)(Int32.Parse(XFUnivElemFormC.GraphValue2) / 5);
            }
	        else
	        {
                ShowMsg("Błąd: Formatka C", "Problem z dostępem do bazy danych. Sprawdź swoje połączenie i ponów próbę.", "OK");
                //GoToZleceniaListPage();
	            GoToMainPage();
            }
	        


	    }


        //async void GetFormDAndSetVariable(string zlecenieCode, string userID, string otherParam, string guid)
        async Task GetFormDAndSetVariable(string zlecenieCode, string userID, string otherParam, string guid)
        {

	        ListFromRestApi = await _myrestDataService.XFZlecenieGetDetailsFormD(zlecenieCode, userID, otherParam, guid);

	        //jesli lista jest pusta to nie pobrano danych z RestApi
	        if (ListFromRestApi.ListaElements.Count != 0)
            {
                //zamien na "" jesli FontSize=0
                ListFromRestApi.ClearNullFieldOfList();

                //ustaw nagłowki etykiet i ich własności
                HeaderFormD.HeaderList = ListFromRestApi.ListaElements[0];     //nagłowki etykiet
                HeaderFormD.HeaderFontProperty = new ViewFontEtykProperty(ListFromRestApi);                        //własności fontów header

                //usun pierwszy odczytany wiersz - nagłowki
                ListFromRestApi.ListaElements.RemoveAt(0);

                //Wyczyszczenie zbędnych null i wrzucenie pierwszego wiersza listy do ViewProperty
                //ClearNullField(MyDatas);
                ListFromRestApi.ClearNullFieldOfList();

                //Do obiektów obserwowanych wstaw tylko drugi rekord - będzie sprawniej - po odcięciui 1-go elem to element 0
                XFListElemFormD = ListFromRestApi.ListaElements;
            }
	        else
	        {
                ShowMsg("Błąd: Formatka D", "Problem z dostępem do bazy danych. Sprawdź swoje połączenie i ponów próbę.", "OK");
                //GoToZleceniaListPage();
	            GoToMainPage();
            }



	    }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {


        }

        //public void OnNavigatedTo(NavigationParameters parameters)
        public async void OnNavigatedTo(NavigationParameters parameters)
        {
            //Element klikniety na liscie - zawiera parametry wywolania procedur
            if (parameters.ContainsKey("myItem"))
            {
                XFUnivElemDetails = (XFUnivElement)parameters["myItem"];
            }

            //pobranie naglowka zbedne - beda tu 4 naglowki dla kazdego FormA-D
            //if (parameters.ContainsKey("myHeader"))
            //{
            //    HeaderDetails = (ViewProperty)parameters["myHeader"];
            //}

            if (parameters.ContainsKey("title"))
            {
                Title = "Zlecenie: "+(string)parameters["title"];
            }

            await GetFormAAndSetVariable(XFUnivElemDetails.E1, AppState.User, "", AppState.Guid);
            await GetFormBAndSetVariable(XFUnivElemDetails.E1, AppState.User, "", AppState.Guid);
            await GetFormCAndSetVariable(XFUnivElemDetails.E1, AppState.User, "", AppState.Guid);
            await GetFormDAndSetVariable(XFUnivElemDetails.E1, AppState.User, "", AppState.Guid);
            //GetFormAAndSetVariable(XFUnivElemDetails.E1, AppState.User, "", AppState.Guid);
            //GetFormBAndSetVariable(XFUnivElemDetails.E1, AppState.User, "", AppState.Guid);
            //GetFormCAndSetVariable(XFUnivElemDetails.E1, AppState.User, "", AppState.Guid);
            //GetFormDAndSetVariable(XFUnivElemDetails.E1, AppState.User, "", AppState.Guid);


        }
    }
}
