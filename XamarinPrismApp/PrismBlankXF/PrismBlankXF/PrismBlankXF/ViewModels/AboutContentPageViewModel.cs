﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using Xamarin.Forms;

namespace PrismBlankXF.ViewModels
{
	public class AboutContentPageViewModel : BindableBase
	{
	    public ICommand OpenWebCommand { get; }

        public AboutContentPageViewModel()
        {
            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("http://www.datainfo.com.pl/")));
        }

	    

	}
}
