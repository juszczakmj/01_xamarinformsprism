﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using PrismBlankXF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Prism.Services;
using PrismBlankXF.Date.Rest;
using PrismBlankXF.LogOn;

namespace PrismBlankXF.ViewModels
{
    public class ViewModelBase : BindableBase, INavigationAware, IDestructible
    {
        TokenService tokenservice = new TokenService();
        TokenKeeper tokenkeeper = new TokenKeeper();
        
        private readonly MyRestDataServiceZlecenia _myrestDataService = new MyRestDataServiceZlecenia();
        XFListaUnivElements ListFromRestApi = new XFListaUnivElements();

        public IPageDialogService _dialogService { get; set; }

        protected INavigationService NavigationService { get; private set; }

        private INavigationService _navigationService;

        public DelegateCommand GoToSecondContentPage { get; private set; }      //niewykorzystywane teraz
        public DelegateCommand CallMsg { get; private set; }
        public DelegateCommand GoToZleceniaListContentPage { get; private set; }  //aktualna nawigacja

        public bool ResultOK = false;    //na razie tak żeby zadziało ShowMsgAkc()

        private bool isPasswordEmpty;
        public bool IsPasswordEmpty
        {
            get { return isPasswordEmpty; }
            set
            {
                //isPasswordEmpty = value;
                //NotifyPropertyChanged();
                SetProperty(ref isPasswordEmpty, value);
            }
        }

        private bool isNameEmpty;
        public bool IsNameEmpty
        {
            get { return isNameEmpty; }
            set
            {
                isNameEmpty = value;
                SetProperty(ref isNameEmpty, value);
                //NotifyPropertyChanged();
            }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _UserPass;
        public string UserPass
        {
            get { return _UserPass; }
            set { SetProperty(ref _UserPass, value); }
        }

        private string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set { SetProperty(ref _UserName, value); }
        }

        private bool _isActive = false;
        public bool IsActive
        {
            get { return _isActive; }
            set { SetProperty(ref _isActive, value); }
        }

        private bool _isAuthorized = false;
        public bool IsAuthorized
        {
            get { return _isAuthorized; }
            set { SetProperty(ref _isAuthorized, value); }
        }

        private bool _isNonAuthorized = false;
        public bool IsNonAuthorized
        {
            get { return _isNonAuthorized; }
            set { SetProperty(ref _isNonAuthorized, value); }
        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            //PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public async void ShowMsg(string title, string mess, string cancel)
        {
            await _dialogService.DisplayAlertAsync(title, mess, cancel);

        }

        //async void GetFormAAndSetVariable(string zlecenieCode, string userID, string otherParam, string guid)
        async Task GetXFMapUserFromAzureAndSetVariable(string user)
        {

                ListFromRestApi = await _myrestDataService.XFMapUserFromAzure(user);

                //jesli lista jest pusta to nie pobrano danych z RestApi
                if (ListFromRestApi.ListaElements.Count != 0)
                {
                    //zamien na "" jesli FontSize=0
                    ListFromRestApi.ClearNullFieldOfList();

                    //usun pierwszy odczytany wiersz - nagłowki
                    ListFromRestApi.ListaElements.RemoveAt(0);

                    //Wyczyszczenie zbędnych null i wrzucenie pierwszego wiersza listy do ViewProperty
                    //ClearNullField(MyDatas);
                    ListFromRestApi.ClearNullFieldOfList();

                    //Do obiektów obserwowanych wstaw tylko drugi rekord - będzie sprawniej - po odcięciui 1-go elem to element 0
                    AppState.User = ListFromRestApi.ListaElements[0].E1;
                    AppState.UserName = ListFromRestApi.ListaElements[0].E2;

                }
                else
                {
                    ShowMsg("Błąd mapowania user", "Problem z mapowaniem usera w bazie SQL", "OK");
                    //GoToMainPage();
                    _navigationService.NavigateAsync($"MainPage");

                }

        }


        public ViewModelBase(INavigationService navigationService, IPageDialogService dialogService)
        {
            _navigationService = navigationService;
            _dialogService = dialogService;

            //ustawienie własności obsrwowania zmiennej IsActive i właczania lub wyłączania przełącznika
            //GoToSecondContentPage = new DelegateCommand(GoToSecondContentPageNavigation).ObservesCanExecute(() => IsActive);
            //GoToZleceniaListContentPage = new DelegateCommand(GoToZleceniaListContentPageNavigation).ObservesCanExecute(() => IsActive);
            
            //bez przełącznika - zamiast IsActive można dodać sprawdzanie poprawności logowania
            GoToZleceniaListContentPage = new DelegateCommand(GoToZleceniaListContentPageNavigation);

            CallMsg = new DelegateCommand(ShowMsg);

        }


        public async void ShowMsg()
        {
            bool result = await _dialogService.DisplayAlertAsync("POTWIERDZENIE", "Czy zatwierdzasz wybraną operację?", "Zatwierdź", "Anuluj");
            //result = result;    //test do wywołania okna Msg
        }

        public async void ShowMsgAkc()
        {
            ResultOK = await _dialogService.DisplayAlertAsync("POTWIERDZENIE", "Czy zatwierdzasz wybraną operację?", "Zatwierdź", "Anuluj");
        }

        public void ShowMsgNoAkc(string tytul, string msg, string key)
        {
            _dialogService.DisplayAlertAsync(tytul, msg, key);
        }

        public virtual void Destroy()
        {

        }

        /// <summary>
        /// niewykorzystywane
        /// </summary>
        public void GoToSecondContentPageNavigation()
        {

            var par = new NavigationParameters();

            par.Add("par1", "val1");
            par.Add("par2", "val2");

            RestAccess.User = UserName;
            RestAccess.Pass = UserPass;

            //Na razie fejkowe wartości tu podstawiamy
            AppState.Guid = "mobile";


            //AppState.User = UserCode;
            //AppState.CurrentCode = UserName;

            _navigationService.NavigateAsync("SecondContentPage", par);

        }

        /// <summary>
        /// aktualna nawigacja
        /// </summary>
        public async void GoToZleceniaListContentPageNavigation()
        {

            var par = new NavigationParameters();
            
            par.Add("par1", "val1");
            par.Add("par2", "val2");

            RestAccess.User = UserName;
            RestAccess.Pass = UserPass;

            //Logowanie - wyczyść tokeny
            //RestAccess.Access_token = "";
            //RestAccess.Refresh_token = "";

            //TokenService tokenservice=new TokenService();                       //ustawienie RestAccess(Access_token,Refresh_token)
            //var tokenserviceGetTokenPublic = tokenservice.GetTokenPublic;

            await tokenservice.GetToken();

            if (RestAccess.Access_token != "" && RestAccess.Access_token != null)
            {
                await GetXFMapUserFromAzureAndSetVariable(RestAccess.User);   //pobranie ID Usera w bazach SQL produkcji
                await _navigationService.NavigateAsync("ZleceniaListContentPage", par);
                IsAuthorized = true;
                IsNonAuthorized = false;
            }
            else
            {
                //ShowMsgNoAkc("Błąd Autoryzacji...", "Błędna nazwa użytkownika lub niewłaściwe hasło", "OK");
                IsAuthorized = false;
                IsNonAuthorized = true;
            }


        }


        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
            //UserName = "";
            UserPass = "";
            //tokenservice.Logout();
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
            if (parameters.ContainsKey("title"))
            {
                Title = "Obecnie: " + (string)parameters["title"];
            }
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }
    }
}
