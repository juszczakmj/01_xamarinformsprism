﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Prism.Navigation;
using PrismBlankXF.Models;
using System.Threading.Tasks;
using Prism.Services;
using PrismBlankXF.Date.Rest;
using PrismBlankXF.LogOn;
using PrismBlankXF.Views;
using Xamarin.Forms;

namespace PrismBlankXF.ViewModels
{
    public class ZleceniaListContentPageViewModel : BindableBase, INavigatedAware
    {
        //private readonly MyRestDataService _myrestDataService = new MyRestDataService();
        private readonly MyRestDataServiceZlecenia _myrestDataService = new MyRestDataServiceZlecenia();

        private DelegateCommand<XFUnivElement> _selItemCommand;    //do obsługi ItemTapped w listView

        // Do wykresu
        //////private int _procent;  //% do wyswietlenia na wykresie
        //////public int Procent
        //////{
        //////    get { return _procent; }
        //////    set { SetProperty(ref _procent, value); }
        //////}

        //////private int _colorstop1;  //% koloru do wyswietlenia na wykresie
        //////public int Colorstop1
        //////{
        //////    get { return _colorstop1; }
        //////    set { SetProperty(ref _colorstop1, value); }
        //////}

        //////private int _colorstop2;  //% koloru do wyswietlenia na wykresie
        //////public int Colorstop2
        //////{
        //////    get { return _colorstop2; }
        //////    set { SetProperty(ref _colorstop2, value); }
        //////}
        // END Do wykresu


        XFListaUnivElements ListFromRestApi = new XFListaUnivElements();
        //nagłowki etykiet i ich własności
        ViewProperty HeadProp = new ViewProperty();

        public IPageDialogService _dialogService { get; set; }

        //własności fontów etykiet Size i atrib
        //ViewFontEtykProperty FProp = new ViewFontEtykProperty(); 
        private ViewFontEtykProperty _fProp;
        public ViewFontEtykProperty FProp
        {
            get { return _fProp; }
            set { SetProperty(ref _fProp, value); }
        }

        private List<XFUnivElement> _mydatas;
        public List<XFUnivElement> MyDatas
        //private XFListaUnivElements _mydatas;
        //public XFListaUnivElements MyDatas
        {
            get { return _mydatas; }
            set { SetProperty(ref _mydatas, value); }
        }

        private readonly INavigationService _navigationService;

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _UserPass;
        public string UserPass
        {
            get { return _UserPass; }
            set { SetProperty(ref _UserPass, value); }
        }

        private string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set { SetProperty(ref _UserName, value); }
        }


        public async void ShowMsg(string title, string mess, string cancel)
        {
           await _dialogService.DisplayAlertAsync(title, mess, cancel);
            
        }

        public void GoToMainPage()
        {
            _navigationService.NavigateAsync($"MainPage");
        }

        //async void RefreshData(string machineCode, string userID)
        async void GetListaZlecenAndSetVariable(string machineCode, string userID, string otherParam, string guid)
        {

            //MyDatas = await _myrestDataService.GetAllWithParam(machineCode, userID);

            //potrzeba tylko userID, ale żeby działała procedrua musze przekazac wszystko
            //więc na razie fejkowe pozostałe dane machineCode,otherParam,guid
            ListFromRestApi = await _myrestDataService.XFZleceniaGetListaA(machineCode, userID, otherParam, guid);

            //jesli lista jest pusta to nie pobrano danych z RestApi
            if (ListFromRestApi.ListaElements.Count!=0)
            {
                //ustawienie własności Size i Atrib dla czcionki etykiet
                FProp = new ViewFontEtykProperty(ListFromRestApi);

                //ustaw nagłowki etykiet i ich własności
                HeadProp.HeaderList = ListFromRestApi.ListaElements[0];     //nagłowki etykiet
                HeadProp.HeaderFontProperty = FProp;                        //własności fontów header

                //usun pierwszy odczytany wiersz - nagłowki
                ListFromRestApi.ListaElements.RemoveAt(0);

                //Wyczyszczenie zbędnych null i wrzucenie pierwszego wiersza listy do ViewProperty
                //ClearNullField(MyDatas);
                ListFromRestApi.ClearNullFieldOfList();

                //Do obiektów obserwowanych wstaw tylko listę - będzie sprawniej
                MyDatas = ListFromRestApi.ListaElements;
            }
            else
            {
                //TU JEST PROBLEM: ALERT+POOWRÓT DO LOGOWANIA
                ShowMsg("Błąd: Pobranie listy zleceń", "Problem z dostępem do bazy danych. Sprawdź swoje połączenie i ponów próbę.", "OK");
                GoToMainPage();


            }



        }


        /// <summary>
        /// KONSTRUKTOR
        /// </summary>
        public ZleceniaListContentPageViewModel(INavigationService navigationService, IPageDialogService dialogService)
        {
            _navigationService = navigationService;
        //Zmienna do Msg
        _dialogService = dialogService;

        UserName = RestAccess.User;
            UserPass = RestAccess.Pass;

            //MyDatas = new ObservableCollection<UnivElement>();
            MyDatas = new List<XFUnivElement>();
            //MyDatas = new XFListaUnivElements() ;

            //Header = new UnivElement();

            //potrzeba tylko userID, ale żeby działała procedrua musze przekazac wszystko
            //więc na razie fejkowe pozostałe dane machineCode,otherParam,guid
            //GetListaZlecenAndSetVariable("", RestAccess.User, "brak", AppState.Guid);
            GetListaZlecenAndSetVariable("", AppState.User, "brak", AppState.Guid);


            //Title = "Lista pozycji - web";

        }

        public DelegateCommand<XFUnivElement> ItemTappedCommand => _selItemCommand ?? (_selItemCommand = new DelegateCommand<XFUnivElement>(ShowMyDateDetails));

        private async void ShowMyDateDetails(XFUnivElement paramData)
        {

            var parameters = new NavigationParameters
            {
                {"myItem", paramData },     //lista do wyswietlenia
	            //{"myHeader",ViewProperty.HeaderList},        //naglowek do nazwy elementow w PageDetails
                //przekazanie naglowka raczej zbedne - na kolejnym ekranie beda 4 zmienne z naglowkami
                //{"myHeader",HeadProp},
                {"title", paramData.E1 }
                //{"title", RestAccess.User }
            };

            await _navigationService.NavigateAsync("ZlecenieDetailsContentPage", parameters);


        }


        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {

            //throw new NotImplementedException();
            if (parameters.ContainsKey("par1"))
            {
                string par1 = (string)parameters["par1"];
                string par2 = (string)parameters["par2"];
            }
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }
    }
}
