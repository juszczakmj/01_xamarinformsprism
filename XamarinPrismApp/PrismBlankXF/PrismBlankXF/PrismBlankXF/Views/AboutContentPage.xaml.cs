﻿using Xamarin.Forms;

namespace PrismBlankXF.Views
{
    public partial class AboutContentPage : ContentPage
    {
        public AboutContentPage()
        {
            InitializeComponent();

            Title = "Info";
        }
    }
}
