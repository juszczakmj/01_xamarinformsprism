﻿using System;
using System.Collections.Generic;
using System.Text;
using PrismBlankXF.Models;
using Xamarin.Forms.Xaml;

namespace PrismBlankXF.Date
{
    public static class MyDateService
    {
        //publiczna statyczna klasa symulująca pozyskiwanie danych
        public static IEnumerable<MyDate> GetAll()
        {
            Random los = new Random(1000);

            List<MyDate> listawyn = new List<MyDate>();  

            for (int i = 0; i < 30; i++)
            {
                int nextlos = los.Next(600, 1000);
                
                listawyn.Add(new MyDate
                {
                    Name = "Element " + (i + 1).ToString(),
                    Comment = "Opis elementu " + (i + 1).ToString(),

                    MaxValue = nextlos,
                    CurValue = los.Next(400,nextlos),
                
                    //ImageUrl = "emotikon1.png"
                    ImageUrl = "Lista.png"
                });
            }

            return listawyn;
        }



    }
    
}
