﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CommonServiceLocator;
using Newtonsoft.Json;
using PrismBlankXF.Models;

namespace PrismBlankXF.Date.Rest
{
    class MyRestDataService
    {
        HttpClient client;

        public MyRestDataService()
        {
            client=new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<List<UnivElement>> GetAll()
        {
            List<UnivElement> Items = new List<UnivElement>();
            string RestUrl = string.Format("{0}/api/ZasobyGetListaA", RestAccess.HostUrl);
            var uri = new Uri(string.Format(RestUrl, string.Empty));
                
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Items = JsonConvert.DeserializeObject<List<UnivElement>>(content);
            }

            return Items;
        }

        public async Task<List<UnivElement>> GetAllWithParam(string machineCode, string userID)
        {
            ListaUnivElements Items = new ListaUnivElements();
            List<UnivElement> ItemsUnivElem = new List<UnivElement>();

            string RestUrl = string.Format("{0}/api/ZasobyGetListaA?machineCode={1}&userID={2}", RestAccess.HostUrl, machineCode, userID);
            var uri = new Uri(string.Format(RestUrl, string.Empty));

            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<ListaUnivElements>(content);

                    ItemsUnivElem = Items.ListaElements;
                }

                return ItemsUnivElem;
            }
            catch
            {
                return null;
            }

            return null;
        }

    }
}
