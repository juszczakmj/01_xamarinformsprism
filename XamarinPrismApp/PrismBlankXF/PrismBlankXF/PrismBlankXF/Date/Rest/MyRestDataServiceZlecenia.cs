﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PrismBlankXF.LogOn;
using PrismBlankXF.Models;

namespace PrismBlankXF.Date.Rest
{
    class MyRestDataServiceZlecenia
    {
        HttpClient client;
        TokenService tokenservice = new TokenService();

        public MyRestDataServiceZlecenia()
        {
            //client = new HttpClient();
            //client.MaxResponseContentBufferSize = 256000;
        }


        //nie wykorzystuję tej metody bez parametrów
        public async Task<List<XFUnivElement>> GetAll()     
        {
            client = new HttpClient();

            List<XFUnivElement> Items = new List<XFUnivElement>();
            string RestUrl = string.Format("{0}/api/XFZleceniaGetListaA", RestAccess.HostUrl);
            //var uri = new Uri(string.Format(RestUrl, string.Empty));

            try
            {
                //Dodanie tokenu
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);

                var uri = new Uri(string.Format(RestUrl, string.Empty));
                var response = await client.GetAsync(uri);

                if ((int)response.StatusCode == 401)
                {
                    tokenservice = new TokenService();
                    await tokenservice.RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);
                    response = await client.GetAsync(uri);
                }

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<List<XFUnivElement>>(content);
                }


                return Items;
            }
            catch (Exception e)
            {
                return Items;
            }


        }

        public async Task<XFListaUnivElements> XFMapUserFromAzure(string user)
        {
            client = new HttpClient();
            XFListaUnivElements Items = new XFListaUnivElements();

            string RestUrl = string.Format("{0}/api/XFMapUserFromAzure?prefixAzure={1}", RestAccess.HostUrl, user);
            //var uri = new Uri(string.Format(RestUrl, string.Empty));

            try
            {
                //Dodanie tokenu
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);

                var uri = new Uri(string.Format(RestUrl, string.Empty));
                var response = await client.GetAsync(uri);

                if ((int)response.StatusCode == 401)
                {
                    tokenservice = new TokenService();
                    await tokenservice.RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);
                    response = await client.GetAsync(uri);
                }

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<XFListaUnivElements>(content);

                }

                return Items;
            }
            catch
            {
                //return null;
                return Items;
            }

            //return null;
        }

        //public async Task<List<XFUnivElement>> XFZleceniaGetListaA(string machineCode, string userID, string otherParam, string guid)
        public async Task<XFListaUnivElements> XFZleceniaGetListaA(string machineCode, string userID, string otherParam, string guid)
        {
            client = new HttpClient();
            XFListaUnivElements Items = new XFListaUnivElements();
            //List<XFUnivElement> ItemsUnivElem = new List<XFUnivElement>();

            string RestUrl = string.Format("{0}/api/XFZleceniaGetListaA?machineCode={1}&userID={2}&otherParam={3}&guid={4}", RestAccess.HostUrl, machineCode, userID, otherParam, guid);
            //var uri = new Uri(string.Format(RestUrl, string.Empty));

            
            try
            {
                //TokenService tokenservice = new TokenService();
                //tokenservice.GetTokenPublic();    //tu właściwie nie będzie GetToken tylko po logowaniu a po wywołaniu poniżej jesli status 401
                //to bedzie RefreshToken

                //Dodanie tokenu
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);
                
                var uri = new Uri(string.Format(RestUrl, string.Empty));
                var response = await client.GetAsync(uri);

                if ((int)response.StatusCode == 401)
                {
                    //TokenService tokenservice = new TokenService();
                    //var tokenserviceRefreshTokenPublic = tokenservice.RefreshTokenPublic;
                    tokenservice = new TokenService();
                    await tokenservice.RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);
                    response = await client.GetAsync(uri);
                }

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<XFListaUnivElements>(content);

                    //ItemsUnivElem = Items.ListaElements;
                }

                //24.02.2019 TU ZACZAC ZMIANE:
                //powinnismy zwrocic Item teraz a nie sama liste XFUnivElements
                //potem zmiany w obiektach obserwowanych i Views

                //return ItemsUnivElem;
                return Items;
            }
            catch
            {
                //return null;
                return Items;
            }

            //return null;
        }


        public async Task<XFListaUnivElements> XFZlecenieGetDetailsFormA(string zlecenieCode, string userID, string otherParam, string guid)
        {
            client = new HttpClient();
            XFListaUnivElements Items = new XFListaUnivElements();

            string RestUrl = string.Format("{0}/api/XFZlecenieGetDetailsFormA?zlecenieCode={1}&userID={2}&otherParam={3}&guid={4}", RestAccess.HostUrl, zlecenieCode, userID, otherParam, guid);
            //var uri = new Uri(string.Format(RestUrl, string.Empty));

            try
            {
                //Dodanie tokenu
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);

                var uri = new Uri(string.Format(RestUrl, string.Empty));
                var response = await client.GetAsync(uri);

                if ((int)response.StatusCode == 401)
                {
                    //TokenService tokenservice = new TokenService();
                    //var tokenserviceRefreshTokenPublic = tokenservice.RefreshTokenPublic;
                    tokenservice = new TokenService();
                    await tokenservice.RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);
                    response = await client.GetAsync(uri);
                }

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<XFListaUnivElements>(content);

                }

                return Items;
            }
            catch
            {
                //return null;
                return Items;
            }

            //return null;
        }


        public async Task<XFListaUnivElements> XFZlecenieGetDetailsFormB(string zlecenieCode, string userID, string otherParam, string guid)
        {
            client = new HttpClient();
            XFListaUnivElements Items = new XFListaUnivElements();

            string RestUrl = string.Format("{0}/api/XFZlecenieGetDetailsFormB?zlecenieCode={1}&userID={2}&otherParam={3}&guid={4}", RestAccess.HostUrl, zlecenieCode, userID, otherParam, guid);
            //var uri = new Uri(string.Format(RestUrl, string.Empty));

            try
            {
                //Dodanie tokenu
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);

                var uri = new Uri(string.Format(RestUrl, string.Empty));
                var response = await client.GetAsync(uri);

                if ((int)response.StatusCode == 401)
                {
                    //TokenService tokenservice = new TokenService();
                    //var tokenserviceRefreshTokenPublic = tokenservice.RefreshTokenPublic;
                    tokenservice = new TokenService();
                    await tokenservice.RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);
                    response = await client.GetAsync(uri);
                }

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<XFListaUnivElements>(content);

                }

                return Items;
            }
            catch
            {
                //return null;
                return Items;
            }

            //return null;
        }


        public async Task<XFListaUnivElements> XFZlecenieGetDetailsFormC(string zlecenieCode, string userID, string otherParam, string guid)
        {
            client = new HttpClient();
            XFListaUnivElements Items = new XFListaUnivElements();

            string RestUrl = string.Format("{0}/api/XFZlecenieGetDetailsFormC?zlecenieCode={1}&userID={2}&otherParam={3}&guid={4}", RestAccess.HostUrl, zlecenieCode, userID, otherParam, guid);
            //var uri = new Uri(string.Format(RestUrl, string.Empty));

            try
            {
                //Dodanie tokenu
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);

                var uri = new Uri(string.Format(RestUrl, string.Empty));
                var response = await client.GetAsync(uri);

                if ((int)response.StatusCode == 401)
                {
                    //TokenService tokenservice = new TokenService();
                    //var tokenserviceRefreshTokenPublic = tokenservice.RefreshTokenPublic;
                    tokenservice = new TokenService();
                    await tokenservice.RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);
                    response = await client.GetAsync(uri);
                }

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<XFListaUnivElements>(content);

                }

                return Items;
            }
            catch
            {
                //return null;
                return Items;
            }

            //return null;
        }


        public async Task<XFListaUnivElements> XFZlecenieGetDetailsFormD(string zlecenieCode, string userID, string otherParam, string guid)
        {
            client = new HttpClient();
            XFListaUnivElements Items = new XFListaUnivElements();

            string RestUrl = string.Format("{0}/api/XFZlecenieGetDetailsFormD?zlecenieCode={1}&userID={2}&otherParam={3}&guid={4}", RestAccess.HostUrl, zlecenieCode, userID, otherParam, guid);
            //var uri = new Uri(string.Format(RestUrl, string.Empty));

            try
            {
                //Dodanie tokenu
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);

                var uri = new Uri(string.Format(RestUrl, string.Empty));
                var response = await client.GetAsync(uri);

                if ((int)response.StatusCode == 401)
                {
                    //TokenService tokenservice = new TokenService();
                    //var tokenserviceRefreshTokenPublic = tokenservice.RefreshTokenPublic;
                    tokenservice = new TokenService();
                    await tokenservice.RefreshToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", RestAccess.Access_token);
                    response = await client.GetAsync(uri);
                }

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<XFListaUnivElements>(content);

                }

                return Items;
            }
            catch
            {
                //return null;
                return Items;
            }

            //return null;
        }


    }
}
