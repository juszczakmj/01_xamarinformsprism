﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using PrismBlankXF.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PrismBlankXF.Converters
{
    public class ItemTappedEventArgsConverter : IValueConverter, IMarkupExtension
    {

        /// <summary>
        /// DO WYKORZYSTANIA W LIST SYNCFUSION
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //var itemTappedEventArgs = value as ItemTappedEventArgs;   -bez SyncFusion
            var itemTappedEventArgs = value as Syncfusion.ListView.XForms.ItemTappedEventArgs;
            if (itemTappedEventArgs == null)
            {
                throw new ArgumentException("Expected value to be of type ItemTappedEventArgs", nameof(value));
            }

            //return itemTappedEventArgs.Item;                          -bez SyncFusion
            return itemTappedEventArgs.ItemData;

        }

        /// <summary>
        /// DO WYKORZYSTANIA W LISTACH BEZ SYNCFUSION
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        //public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        //{
        //    var itemTappedEventArgs = value as ItemTappedEventArgs;
        //    if (itemTappedEventArgs == null)
        //    {
        //        throw new ArgumentException("Expected value to be of type ItemTappedEventArgs", nameof(value));
        //    }

        //    return itemTappedEventArgs.Item;

        //}

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
