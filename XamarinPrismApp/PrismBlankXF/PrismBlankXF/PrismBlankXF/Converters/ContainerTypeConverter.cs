﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Syncfusion.XForms.TextInputLayout;
using Xamarin.Forms;

namespace PrismBlankXF.Converters
{
    /// <summary>
    /// To get the ContainerType
    /// </summary>
    public class ContainerTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var type = value as string;
            return type == "Outlined" ? ContainerType.Outlined : ContainerType.Filled;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
