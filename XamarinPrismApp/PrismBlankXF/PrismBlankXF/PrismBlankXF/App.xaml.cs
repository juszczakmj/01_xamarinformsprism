﻿using Prism;
using Prism.Ioc;
using PrismBlankXF.ViewModels;
using PrismBlankXF.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Unity;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace PrismBlankXF
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            //Register Syncfusion license
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("Njg0NDZAMzEzNjJlMzQyZTMwbENqSXVKWWhEbmZma0hvbFR5WjU4eTR4a0Z5L2t5dmUwZHJLZ1Y0WFNrRT0=");
            
            InitializeComponent();

            await NavigationService.NavigateAsync("NavigationPage/MainPage");
            //await NavigationService.NavigateAsync("NavigationPage/CardsTabbedPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage>();
            containerRegistry.RegisterForNavigation<CardsTabbedPage>(); //niewykorzystywana obecnie strona zakłądek. Wystarczy uruchomić i dodać podstrony
            containerRegistry.RegisterForNavigation<AboutContentPage>();
            containerRegistry.RegisterForNavigation<ZleceniaListContentPage, ZleceniaListContentPageViewModel>();
            containerRegistry.RegisterForNavigation<ZlecenieDetailsContentPage, ZlecenieDetailsContentPageViewModel>();
        }
    }
}
