﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrismBlankXF.Models
{
    public static class RestAccess
    {
        public static string HostUrl = "http://xxxxxxxxxxxxxxxxxxxxxxxx.azurewebsites.net";  //azure bez autoryzacji
        //public static string HostUrl = "http://xxxxxx.hostingasp.pl/restapi_xxxxxxxx";    //webio.pl bez autoryzacji
        //public static string HostUrl = "http://xxxxxxxxostingasp.pl/restapixxxxxxxxx";    //webio.pl z autoryzacja
        public static string User { get; set; }
        public static string Pass { get; set; }

        public static string Client_ID = "xxxxxxxxxxxx";
        public static string Client_secret = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

        public static string Access_token { get; set; }
        public static string Refresh_token { get; set; }

    }
}
