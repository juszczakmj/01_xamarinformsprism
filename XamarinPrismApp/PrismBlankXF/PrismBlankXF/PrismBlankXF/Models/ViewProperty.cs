﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace PrismBlankXF.Models
{
    public class ViewProperty
    {
        //nagłowki etykiet - prsechwycony pierwszy wiersz record set
        public XFUnivElement HeaderList { get; set; }

        //własności etykiet 
        public ViewFontEtykProperty HeaderFontProperty { get; set; }

        /// <summary>
        /// konstruktor
        /// </summary>
        public ViewProperty()
        {
            this.HeaderList = new XFUnivElement();
            this.HeaderFontProperty=new ViewFontEtykProperty();
        }
        
    }
}
