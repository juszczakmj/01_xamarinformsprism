﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrismBlankXF.Models
{
    /// <summary>
    /// Klasa do ListView na SecondPageVieModel
    /// </summary>
    public class MyDate
    {
        public string Name { get; set; }
        public string Comment { get; set; }
        public string ImageUrl { get; set; }
        public int MaxValue { get; set; }
        public int CurValue { get; set; }
    }
}
