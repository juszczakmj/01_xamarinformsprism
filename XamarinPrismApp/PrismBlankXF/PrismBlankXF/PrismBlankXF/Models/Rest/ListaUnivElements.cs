﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PrismBlankXF.Models
{

    /// <summary>
    /// Uniwerslna lista elementów
    /// </summary>
    //[DataContract]
    public class ListaUnivElements:Exec
    {
        //[DataMember(Name = "ListaElements")]
        public List<UnivElement> ListaElements { get; set; }

        //[DataMember(Name = "Tytul")]
        public string Tytul { get; set; }   //tytuł okna do wyświetlenia na formatce

        //ogólna szerokość kolumn otrzymanych z select - indeks szerokości Width0-Width5 odpowiada indeksowi kolumny C0-C5
        //jeśli 0  to jej nie ustawiać
        //[DataMember(Name = "Width0")]
        public int Width0 { get; set; }

        //[DataMember(Name = "Width1")]
        public int Width1 { get; set; }

        //[DataMember(Name = "Width2")]
        public int Width2 { get; set; }

        //[DataMember(Name = "Width3")]
        public int Width3 { get; set; }

        //[DataMember(Name = "Width4")]
        public int Width4 { get; set; }

        //[DataMember(Name = "Width5")]
        public int Width5 { get; set; }

        //Dodatkowy parametr, któy mozna wykorzystac do szczególnych ustawien
        public string Parametry_ust { get; set; }

        //konstruktor
        public ListaUnivElements()
        {
            this.ListaElements = new List<UnivElement>();
            this.Tytul = "";
            this.Width0 = 0;
            this.Width1 = 0;
            this.Width2 = 0;
            this.Width3 = 0;
            this.Width4 = 0;
            this.Width5 = 0;
            this.Parametry_ust = "";
        }
    }
}