﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrismBlankXF.Models
{
    /// <summary>
    /// trzymam tylko własnośi etykiet czyli fontsize i fontatrib do XAML
    /// </summary>
    public class ViewFontEtykProperty
    {
        public string Size1 { get; set; }
        public string Atrib1 { get; set; }

        public string Size2 { get; set; }
        public string Atrib2 { get; set; }

        public string Size3 { get; set; }
        public string Atrib3 { get; set; }

        public string Size4 { get; set; }
        public string Atrib4 { get; set; }

        public string Size5 { get; set; }
        public string Atrib5 { get; set; }

        public string Size6 { get; set; }
        public string Atrib6 { get; set; }

        public string Size7 { get; set; }
        public string Atrib7 { get; set; }

        public string Size8 { get; set; }
        public string Atrib8 { get; set; }

        public string Size9 { get; set; }
        public string Atrib9 { get; set; }

        public string Size10 { get; set; }
        public string Atrib10 { get; set; }


        /// <summary>
        /// kostruktor bez parametru
        /// </summary>
        public ViewFontEtykProperty()
        {
            Size1 = "default";
            Atrib1 = "none";

            Size2 = "default";
            Atrib2 = "none";

            Size3 = "default";
            Atrib3 = "none";

            Size4 = "default";
            Atrib4 = "none";

            Size5 = "default";
            Atrib5 = "none";

            Size6 = "default";
            Atrib6 = "none";

            Size7 = "default";
            Atrib7 = "none";

            Size8 = "default";
            Atrib8 = "none";

            Size9 = "default";
            Atrib9 = "none";

            Size10 = "default";
            Atrib10 = "none";

        }

        /// <summary>
        /// konstruktor z parametrem - w lista juz są pobrane wartości Size i Atrib
        /// </summary>
        /// <param name="lista"></param>
        public  ViewFontEtykProperty(XFListaUnivElements lista)
        {
            Size1 = lista.Size1;
            Atrib1 = lista.Atrib1;

            Size2 = lista.Size2;
            Atrib2 = lista.Atrib2;

            Size3 = lista.Size3;
            Atrib3 = lista.Atrib3;

            Size4 = lista.Size4;
            Atrib4 = lista.Atrib4;

            Size5 = lista.Size5;
            Atrib5 = lista.Atrib5;

            Size6 = lista.Size6;
            Atrib6 = lista.Atrib6;

            Size7 = lista.Size7;
            Atrib7 = lista.Atrib7;

            Size8 = lista.Size8;
            Atrib8 = lista.Atrib8;

            Size9 = lista.Size9;
            Atrib9 = lista.Atrib9;

            Size10 = lista.Size10;
            Atrib10 = lista.Atrib10;

        }

        /// <summary>
        /// ustawia na podstawie danych w lista pola Size i Atrib
        /// </summary>
        /// <param name="lista"></param>
        public void SetFontEtykProperty(XFListaUnivElements lista)
        {
            Size1 = lista.Size1;
            Atrib1 = lista.Atrib1;

            Size2 = lista.Size2;
            Atrib2 = lista.Atrib2;

            Size3 = lista.Size3;
            Atrib3 = lista.Atrib3;

            Size4 = lista.Size4;
            Atrib4 = lista.Atrib4;

            Size5 = lista.Size5;
            Atrib5 = lista.Atrib5;

            Size6 = lista.Size6;
            Atrib6 = lista.Atrib6;

            Size7 = lista.Size7;
            Atrib7 = lista.Atrib7;

            Size8 = lista.Size8;
            Atrib8 = lista.Atrib8;

            Size9 = lista.Size9;
            Atrib9 = lista.Atrib9;

            Size10 = lista.Size10;
            Atrib10 = lista.Atrib10;

        }
    }
}
