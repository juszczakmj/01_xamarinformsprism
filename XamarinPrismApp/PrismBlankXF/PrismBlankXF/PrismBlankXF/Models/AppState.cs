﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrismBlankXF.Models
{
    public static class AppState

    {
        public static string User { get; set; }
        public static string UserName { get; set; }
        public static string CurrentCode { get; set; }
        public static string Guid { get; set; }

    }
}
