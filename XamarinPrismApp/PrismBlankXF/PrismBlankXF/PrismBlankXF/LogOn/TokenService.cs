﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PrismBlankXF.Models;

namespace PrismBlankXF.LogOn
{
    class TokenService
    {
        TokenKeeper tokenkeeper;

        public TokenService()
        {
            tokenkeeper = new TokenKeeper();
        }

        //Potem jeszcze usunąć token 
        public void Logout()
        {
            //wyzeruj tokeny w aplikacji - odcinamy dostęp
            RestAccess.Access_token = "";
            RestAccess.Refresh_token = "";

        }

        //pobranie tokenu
        //public async Task<TokenKeeper> GetToken()
        public async Task GetToken()
        {
            string stringuri = string.Format("{0}/token", RestAccess.HostUrl);

            var dict = new Dictionary<string, string>();
            dict.Add("grant_type", "password");
            dict.Add("username", RestAccess.User);
            dict.Add("password", RestAccess.Pass);
            dict.Add("client_id", RestAccess.Client_ID);
            dict.Add("client_secret", RestAccess.Client_secret);

            tokenkeeper = new TokenKeeper();

            var httpclient = new HttpClient();
            try
            {
                //var httpclient = new HttpClient();
                var req = new HttpRequestMessage(HttpMethod.Post, stringuri) { Content = new FormUrlEncodedContent(dict) };
                var res = await httpclient.SendAsync(req);

                if (res.IsSuccessStatusCode)
                {
                    var content = await res.Content.ReadAsStringAsync();
                    tokenkeeper = JsonConvert.DeserializeObject<TokenKeeper>(content);

                    RestAccess.Access_token = tokenkeeper.access_token;
                    RestAccess.Refresh_token = tokenkeeper.refresh_token;

                    //return tokenkeeper;

                }

                //return tokenkeeper;
            }
            catch
            {
                //return tokenkeeper;
            }
            finally
            {
                httpclient.CancelPendingRequests();
                httpclient.Dispose();

            }



        }

        //Odswiezenie tokenu
        //public async Task<TokenKeeper> RefreshToken()
        public async Task RefreshToken()
        {
            string stringuri = string.Format("{0}/token", RestAccess.HostUrl);

            var dict = new Dictionary<string, string>();
            dict.Add("grant_type", "refresh_token");
            dict.Add("client_id", RestAccess.Client_ID);
            dict.Add("client_secret", RestAccess.Client_secret);
            dict.Add("Refresh_token", RestAccess.Refresh_token);

            tokenkeeper = new TokenKeeper();

            var httpclient = new HttpClient();
            try
            {
                //var httpclient = new HttpClient();
                var req = new HttpRequestMessage(HttpMethod.Post, stringuri) { Content = new FormUrlEncodedContent(dict) };
                var res = await httpclient.SendAsync(req);

                if (res.IsSuccessStatusCode)
                {
                    var content = await res.Content.ReadAsStringAsync();
                    tokenkeeper = JsonConvert.DeserializeObject<TokenKeeper>(content);

                    RestAccess.Access_token = tokenkeeper.access_token;
                    RestAccess.Refresh_token = tokenkeeper.refresh_token;

                    //return tokenkeeper;

                }

                //return tokenkeeper;
            }
            catch
            {
                //return tokenkeeper;
            }
            finally
            {
                httpclient.CancelPendingRequests();
                httpclient.Dispose();
            }



        }

    }

}
