﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrismBlankXF.LogOn
{
    /// <summary>
    /// Klasa do przesłania POST i przechwytywania tokenów i RefreshTokenów
    /// Zaraz potem następuje przepisanie wartości do RestAccess
    /// </summary>

   public class TokenKeeper
    {
        //parametry zgodne z wywołaniem metody Token kontrolera
        public string access_token { get; set; }
        public string refresh_token { get; set; }

        //w odpowiednim momencie dane pobrane zostają z RestAccess
        public TokenKeeper()
        {
            access_token = "";
            refresh_token = "";
        }
    }
}
